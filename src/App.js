import './App.css';
import ChessGame from './components/ChessGame/ChessGame';

function App() {
  return (
    <ChessGame />
  );
}

export default App;
