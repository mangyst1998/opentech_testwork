import React from 'react';
import Piece from '../Piece/Piece';
import './chessTable.css';

const ChessTable = (props) => {
    const letters = ['h', 'g', 'f', 'e', 'd', 'c', 'b', 'a'];
    const digits = ['1', '2', '3', '4', '5', '6', '7', '8'];

    return (
        <div className="chessTable-wrapper">
            <div className="letters">{letters.map(item => <span key={item + '-top'}  className="rotate">{item}</span> )}</div>
            <div className="center-container">
                <div className="digits">{digits.map(item => <span key={item + '-left'}>{item}</span> )}</div>
                <table>
                    <tbody>
                        {props.table.map((itemY, indexY) => {
                            let rowData = itemY.map((itemX, indexX) => {
                                let step = indexY % 2 === 0 ? 1 : 0;
                                return (
                                    <td key={indexX} className={(indexX + step) % 2 === 0 ? 'green' : 'white'}>
                                        {
                                            itemX 
                                            ?   <Piece
                                                    key = {indexY + ':' + indexX}
                                                    type = {itemX}
                                                    y = {indexY + 1}
                                                    x = {letters[indexX]}
                                                />
                                            :   null
                                        }
                                    </td>
                                )
                                
                            })
                            return <tr key={indexY}>{rowData}</tr>
                        })}
                    </tbody>
                </table>
                <div className="digits">{digits.map(item => <span key={item + '-right'} className="rotate">{item}</span> )}</div>
            </div>
            <div className="letters">{letters.map(item => <span key={item + '-bottom'}>{item}</span> )}</div>
        </div>
    )
}

export default ChessTable;