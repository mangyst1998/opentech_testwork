import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { newGameAction, pauseAction, startAction } from '../reducer/reducer';
import './buttons.css';

const Buttons = () => {
    const emptyTable = [
        ['','','','','','','',''],
        ['','','','','','','',''],
        ['','','','','','','',''],
        ['','','','','','','',''],
        ['','','','','','','',''],
        ['','','','','','','',''],
        ['','','','','','','',''],
        ['','','','','','','','']
      ]

    const dispatch = useDispatch();
    const {newGame, active} = useSelector(state => state);

    const newSimulation = () => {
        if(!active){
        
            dispatch(newGameAction({table: emptyTable, newGame: !newGame}));
        }
    }

    const start = () => {
        if(!active){
            dispatch(startAction());
        }
    }

    const pause = () => {
        if(active){
            dispatch(pauseAction());
        }
    }

    return (
        <div className="buttons-wrapper">
            <div className="new-game">
                <span className={active ? "tooltip-new-game" : ""}>Pause the game first</span>
                <button onClick={() => newSimulation()}>NEW SIMULATION</button>
            </div>
            <button onClick={() => start()}>START</button>
            <button onClick={() => pause()}>PAUSE</button>
        </div>
    )
}

export default Buttons;