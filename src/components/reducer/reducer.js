const emptyTable = [
    ['','','','','','','',''],
    ['','','','','','','',''],
    ['','','','','','','',''],
    ['','','','','','','',''],
    ['','','','','','','',''],
    ['','','','','','','',''],
    ['','','','','','','',''],
    ['','','','','','','','']
  ]
  
const defaultState = {
    active: false,
    table: emptyTable,
    moves: [],
    position: {},
    activePlayer: 'white',
    newGame: false
}

const enums = {
    START: 'START',
    PAUSE: 'PAUSE',
    NEW_GAME: 'NEW_GAME',
    POSITION: 'POSITION',
    MOVE: 'MOVE',
    REPEAT_FROM: 'REPEAT_FROM'
}


const reducer = (state = defaultState, action) => {
    switch(action.type){
        case enums.START:
            return {...state, active: true};
        case enums.PAUSE:
            return {...state, active: false};
        case enums.NEW_GAME:
            return {...state, newGame: action.payload.newGame, active: false, table: action.payload.table, moves: [], position: {}, activePlayer: 'white'};
        case enums.POSITION:
            return {...state, position: action.payload.position};
        case enums.MOVE:
            return {...state, table: action.payload.table, position: action.payload.position, activePlayer: action.payload.player};
        case enums.REPEAT_FROM:
            return {...state, table: action.payload.table, activePlayer: action.payload.player, active: false, position: action.payload.position, moves: action.payload.moves};
        default:
            return state;
    }
}

export const startAction = () => ({type: enums.START});
export const pauseAction = () => ({type: enums.PAUSE});
export const newGameAction = (payload) => ({type: enums.NEW_GAME, payload});
export const positionAction = (payload) => ({type: enums.POSITION, payload});
export const moveAction = (payload) => ({type: enums.MOVE, payload});
export const repeatFromAction = (payload) => ({type: enums.REPEAT_FROM, payload});

export default reducer;