import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { repeatFromAction } from '../reducer/reducer';

const MoveDetail = (props) => {
    const {moves, active} = useSelector(state => state);
    const dispatch = useDispatch();

    const updateTable = (data, index) => {
        const positions = {};
        
        if(!active){
            data.prevTable.map((item, indexY) =>{
                item.map((cellItem, indexX) => {
                    if(cellItem !== ''){
                        positions[cellItem] = {
                            y: indexY,
                            x: indexX
                        }
                    }
                })
            })
    
            dispatch(repeatFromAction({table: data.prevTable, player: data.player, position: positions,moves: moves.slice(0, index)}));
        }
    }

    return (
        <div id={props.index} className="move" onClick={() => {updateTable(moves[props.index], props.index)}}>
            <div>{props.piece}</div>
            <div>{props.from}</div>
            <div>{props.to}</div>
            <span className="tooltiptext">Remove this and below steps</span>
            <span className={active ? "tooltip-new-game" : ""}>Pause the game first</span>
        </div>
    )
}

export default MoveDetail;