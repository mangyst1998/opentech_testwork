import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Buttons from '../Buttons/Buttons';
import ChessTable from '../ChessTable/ChessTable';
import MoveDetail from '../MoveDetail/MoveDetail';
import { moveAction, positionAction } from '../reducer/reducer';

const ChessGame = () => {
  const {active, table, position, activePlayer, moves, newGame} = useSelector(state => state);
  const dispatch = useDispatch();
  let changePlayer, movesResult;
  let pieceDetail = {};
  
  //Adds pieces to the Chess Table
  const addPieces = () => {
    const pieces = ['white-queen', 'white-bishop', 'white-knight','black-queen', 'black-bishop', 'black-knight'];
    let y,x, steps;

    pieces.forEach(item => {
      steps = 0;
      do{
          y = Math.floor(Math.random() * table.length);
          x = Math.floor(Math.random() * table.length);

          if(table[y][x] === ''){
            table[y][x] = item;

            pieceDetail[item] = {
              y: y,
              x: x
            }
            steps++;
          }

      }while(steps === 0)
    })

    dispatch(positionAction({position: pieceDetail}));
  }
  
  //Gets random Piece of Active Player and call move function
  const movePieces = () => {
    const activePieces = Object.keys(position)
    const whitePieces = activePieces.filter( item => item.split('-')[0] === 'white'),
          blackPieces = activePieces.filter( item => item.split('-')[0] === 'black');

    let piece, selectPiece, mainY, mainX, side, type,
        availablePositions = [],
        mustKillPositions = [];

    if(activePlayer === 'white'){
      selectPiece = Math.floor(Math.random() * whitePieces.length);
      piece = whitePieces[selectPiece];
      changePlayer = 'black';
    }else if(activePlayer === 'black'){
      selectPiece = Math.floor(Math.random() * blackPieces.length);
      piece = blackPieces[selectPiece];
      changePlayer = 'white';
    }

    if(position[piece]){
      mainY = position[piece].y;
      mainX = position[piece].x;
      [side, type] = piece.split('-');
    }

    new Promise( (resolve) => {
      setTimeout(() => resolve(),2000);
    }).then(() => {
        if(type === 'queen'){
          moveQueen(mainY, mainX, side, availablePositions, mustKillPositions)
        }
    
        if(type === 'bishop'){
          moveBishop(mainY, mainX, side, availablePositions, mustKillPositions)
        }
    
        if(type === 'knight'){
          moveKnight(mainY, mainX, side, availablePositions, mustKillPositions)
        }
      }
    )
  }

  //Checks if enemy piece is on the table cell
  const isEnemy = (side, y, x) => {
    if(side === table[y][x].split('-')[0]){
      return 0;
    }else {
      return 1;
    }
  }
  
  //Moves the Piece dependence on is there Enemys to kill or not 
  //and updates the State
  const move = (mainY, mainX, piece,availablePositions, mustKillPositions) => {
    const letters = ['H', 'G', 'F', 'E', 'D', 'C', 'B', 'A'];
    let randomPosition, y, x, moveInfo, prevTable = [];

    if(mustKillPositions.length){
      randomPosition = Math.floor(Math.random() * mustKillPositions.length);
      y = mustKillPositions[randomPosition].y;
      x = mustKillPositions[randomPosition].x;

      table.forEach((item,index) => {
        prevTable[index] = [...item];
      })

      delete position[table[y][x]];
      table[y][x] = piece;
      table[mainY][mainX] = '';
      position[piece].y = y;
      position[piece].x = x;
      
    
      moveInfo = {
        piece: piece,
        from: letters[mainX] + (mainY+1),
        to: letters[x] + (y+1),
        prevTable: prevTable,
        player: changePlayer
      };
      moves.push(moveInfo);

    }else if(availablePositions.length){
      randomPosition = Math.floor(Math.random() * availablePositions.length);
      y = availablePositions[randomPosition].y;
      x = availablePositions[randomPosition].x;

      table.forEach((item,index) => {
        prevTable[index] = [...item];
      })

      table[y][x] = piece;
      table[mainY][mainX] = '';
      position[piece].y = y;
      position[piece].x = x;

      moveInfo = {
        piece: piece,
        from: letters[mainX] + (mainY+1),
        to: letters[x] + (y+1),
        prevTable: prevTable,
        player: changePlayer
      };

      moves.push(moveInfo);
      
    }else {
      console.log('no available cell to move');
    }

    dispatch(moveAction({table: table, position: position, player: changePlayer, moves: moves}));
  }
  
  //gets positions from Current cell to Top Left side by diagonal
  const getPositionToTopLeftSide = (y,x,side, availablePositions,mustKillPositions) => {
    while((x-1) >= 0 && (y-1) >= 0){
      x--;
      y--;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
        x = 0;
        y = 0;
      }
    }

    return [availablePositions,mustKillPositions];
  }
  
  //gets positions from Current cell to Top Right side by diagonal
  const getPositionToTopRightSide = (y,x,side, availablePositions,mustKillPositions) => {
    while((x+1) <= 7 && (y-1) >=0) {
      x++;
      y--;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
        x = 7;
        y = 0;
      }
    }

    return [availablePositions,mustKillPositions];
  }
  
  //gets positions from Current cell to Bottom Left side by diagonal
  const getPositionToBottomLeftSide = (y,x,side, availablePositions,mustKillPositions) => {
    while((x-1) >= 0 && (y+1) <= 7) {
      x--;
      y++;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
        x = 0;
        y = 7;
      }
    }

    return [availablePositions,mustKillPositions];
  }
  
  //gets positions from Current cell to Bottom Right side by diagonal
  const getPositionToBottomRightSide = (y,x,side, availablePositions,mustKillPositions) => {
    while((x+1) <= 7 && (y+1) <= 7) {
      x++;
      y++;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
        x = 7;
        y = 7;
      }
    }

    return [availablePositions,mustKillPositions];
  }
  
  //gets positions from Current cell to Top side by vertical
  const getPositionToTopSide = (y,x,side, availablePositions,mustKillPositions) => {
    while((y-1) >= 0) {
      y--;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
        y = 0;
      }
    }

    return [availablePositions,mustKillPositions];
  }
  
  //gets positions from Current cell to Right side by horizontal
  const getPositionToRightSide = (y,x,side, availablePositions,mustKillPositions) => {
    while((x+1) <= 7) {
      x++;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
        x = 7;
      }
    }

    return [availablePositions,mustKillPositions];
  }
  
  //gets positions from Current cell to Bottom side by vertical
  const getPositionToBottomSide = (y,x,side, availablePositions,mustKillPositions) => {
    while((y+1) <= 7) {
      y++;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
        y = 7;
      }
    }

    return [availablePositions,mustKillPositions];
  }
  
  //gets positions from Current cell to Left side by horizontal
  const getPositionToLeftSide = (y,x,side, availablePositions,mustKillPositions) => {
    while((x-1) >= 0) {
      x--;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
        x = 0;
      }
    }

    return [availablePositions,mustKillPositions];
  }
  
  //gets positions to move and moves Bishop piece
  const moveBishop = (mainY, mainX, side, availablePositions, mustKillPositions) => {
    [availablePositions,mustKillPositions] = getPositionToTopLeftSide(mainY,mainX,side, availablePositions,mustKillPositions);
    [availablePositions,mustKillPositions] = getPositionToTopRightSide(mainY,mainX,side, availablePositions,mustKillPositions);
    [availablePositions,mustKillPositions] = getPositionToBottomLeftSide(mainY,mainX,side, availablePositions,mustKillPositions);
    [availablePositions,mustKillPositions] = getPositionToBottomRightSide(mainY,mainX,side, availablePositions,mustKillPositions);

    move(mainY, mainX,side+'-bishop',availablePositions,mustKillPositions);
  }
  
  //gets positions to move and moves Queen piece
  const moveQueen = (mainY, mainX, side, availablePositions, mustKillPositions) => {
    [availablePositions,mustKillPositions] = getPositionToTopLeftSide(mainY,mainX,side, availablePositions,mustKillPositions);
    [availablePositions,mustKillPositions] = getPositionToTopRightSide(mainY,mainX,side, availablePositions,mustKillPositions);
    [availablePositions,mustKillPositions] = getPositionToBottomLeftSide(mainY,mainX,side, availablePositions,mustKillPositions);
    [availablePositions,mustKillPositions] = getPositionToBottomRightSide(mainY,mainX,side, availablePositions,mustKillPositions);
    [availablePositions,mustKillPositions] = getPositionToTopSide(mainY,mainX,side, availablePositions,mustKillPositions);
    [availablePositions,mustKillPositions] = getPositionToRightSide(mainY,mainX,side, availablePositions,mustKillPositions);
    [availablePositions,mustKillPositions] = getPositionToBottomSide(mainY,mainX,side, availablePositions,mustKillPositions);
    [availablePositions,mustKillPositions] = getPositionToLeftSide(mainY,mainX,side, availablePositions,mustKillPositions);

    move(mainY, mainX,side+'-queen',availablePositions,mustKillPositions);
  }
  
  //gets positions to move and moves Knight piece
  const moveKnight = (mainY, mainX, side, availablePositions, mustKillPositions) => {
    let x,y;
    
    //checking from Position to leftTop
    if((mainY - 1) >= 0 && (mainX - 2) >= 0){
      y = mainY - 1;
      x = mainX - 2;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
      }
    }

    //checking from Position to leftBottom
    if((mainY + 1) <= 7 && (mainX - 2) >= 0){
      y = mainY + 1;
      x = mainX - 2;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
      }
    }

    //checking from Position to topLeft
    if((mainY - 2) >= 0 && (mainX - 1) >= 0){
      y = mainY - 2;
      x = mainX - 1;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
      }
    }

    //checking from Position to topRight
    if((mainY - 2) >= 0 && (mainX + 1) <= 7){
      y = mainY - 2;
      x = mainX + 1;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
      }
    }

    //checking from Position to rightTop
    if((mainY - 1) >= 0 && (mainX + 2) <= 7){
      y = mainY - 1;
      x = mainX + 2;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
      }
    }

    //checking from Position to rightBottom
    if((mainY + 1) <= 7 && (mainX + 2) <= 7){
      y = mainY + 1;
      x = mainX + 2;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
      }
    }

    //checking from Position to bottomLeft
    if((mainY + 2) <= 7 && (mainX - 1) >= 0){
      y = mainY + 2;
      x = mainX - 1;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
      }
    }

    //checking from Position to bottomRight
    if((mainY + 2) <= 7 && (mainX + 1) <= 7){
      y = mainY + 2;
      x = mainX + 1;
      if(table[y][x] === '') {
        availablePositions.push({y: y, x: x});
      }else {
        if(isEnemy(side, y, x)){
          mustKillPositions.push({y: y, x: x});
        }
      }
    }
  
    move(mainY, mainX,side+'-knight',availablePositions,mustKillPositions);
  }

  useEffect( () => {
    addPieces();
  }, [newGame])
 

  if(active){
    movePieces();
  }

  movesResult = moves.map((item,index) => {
    return <MoveDetail 
      key = {index}
      index = {index}
      piece = {item.piece}
      from = {item.from}
      to = {item.to}
    />
  })

  return (
    <section>
      <div className="player">
        <h2>
          Player A
        </h2>
        <div className="pieces pieces-white">
          <div className={!position['white-knight'] ? 'knight dead' : 'knight'}></div>
          <div className={!position['white-bishop']  ? 'bishop dead' : 'bishop'}></div>
          <div className={!position['white-queen'] ? 'queen dead' : 'queen'}></div>
        </div>
        <div>
          {!position['black-knight'] && !position['black-bishop'] && !position['black-queen'] ? 'WINNER' : ''}
        </div>
      </div>
      <div className="game">
        <ChessTable 
          table = {table}
        />
        <Buttons />

        <div className="moves">
          <div className="moves-header">
            <div>
              Piece
            </div>
            <div>
              from
            </div>
            <div>
              to
            </div>
          </div>
          <div className="moves-results">
            {
              movesResult
            }
          </div>
        </div>
      </div>
      <div className="player">
        <h2>
          Player B
        </h2>
        <div className="pieces pieces-black">
          <div className={!position['black-knight'] ? 'knight dead' : 'knight'}></div>
          <div className={!position['black-bishop'] ? 'bishop dead' : 'bishop'}></div>
          <div className={!position['black-queen'] ? 'queen dead' : 'queen'}></div>
        </div>
        <div>
        {!position['white-knight'] && !position['white-bishop'] && !position['white-queen'] ? 'WINNER' : ''}
        </div>
      </div>
    </section>
  );
}


export default ChessGame;